import secrets
import uuid
from datetime import datetime
from typing import Any, Annotated

from fastapi import APIRouter, Depends, Response, Request, Cookie, HTTPException, status
from fastapi.security import HTTPBasicCredentials, HTTPBasic


router = APIRouter(prefix="/api/v1", tags=["v1_auth_cookies"])

security = HTTPBasic()

COOKIES_SESSION_ID_KEY = "app_session_id"

TOKEN_LIFE = 60

COOKIES: dict[str : dict[str:Any]] = {}
BASE = {
    "user": "user",
    "admin": "admin",
}


def get_auth_user(credentials: Annotated[HTTPBasicCredentials, Depends(security)]):
    unauth_exc = HTTPException(
        status.HTTP_401_UNAUTHORIZED,
        detail="invalid username or password",
        headers={"WWW-Authenticate": "Basic"},
    )
    correct_password = BASE.get(credentials.username)
    if correct_password is None:
        raise unauth_exc
    if credentials.username not in BASE:
        raise unauth_exc
    if not secrets.compare_digest(
        credentials.password.encode("utf-8"), correct_password.encode("utf-8")
    ):
        raise unauth_exc
    return credentials.username


def generate_session_id() -> str:
    return uuid.uuid4().hex


@router.post("/login/")
def auth_login_cookie(response: Response, auth_username: str = Depends(get_auth_user)):
    session_id = generate_session_id()
    response.set_cookie(COOKIES_SESSION_ID_KEY, session_id)
    COOKIES[session_id] = {"token_date": datetime.now()}
    COOKIES[session_id] = {"token": "user_token"}
    return {"resalt": "ok"}


@router.get("/logout/")
def auth_logout_cookie(
    response: Response, session_id: str = Cookie(alias=COOKIES_SESSION_ID_KEY)
):
    COOKIES.pop(session_id)
    response.delete_cookie(COOKIES_SESSION_ID_KEY)
    return {"result": "ok"}


def verifi_token(
    response: Response, session_id: str = Cookie(alias=COOKIES_SESSION_ID_KEY)
) -> User:
    if session_id not in COOKIES:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="not authentication"
        )
    user_session_data = COOKIES[session_id]  # token data
    token = user_session_data["token"]
    token_date = user_session_data["token_date"]
    if not token or not token_date or datetime.now() - token_date > TOKEN_LIFE:
        COOKIES.pop(session_id)
        response.delete_cookie(COOKIES_SESSION_ID_KEY)
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token"
        )
    return  # User


@router.get("/check-salary/")
def get_user_salary(user: User = Depends(verifi_token)) -> dict:
    return {
        "user": User.name,
        "salary": User.salary,
        "date_salary_increase": User.date_salary_increase,
    }
