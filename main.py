import uvicorn
from fastapi import FastAPI
from core.api_v1 import router as auth_cookie_router

app = FastAPI()

app.include_router(auth_cookie_router)


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
