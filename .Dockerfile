FROM python:3.11-slim
COPY . .
RUN pip install poetry
RUN poetry shell
RUN poetry install

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]